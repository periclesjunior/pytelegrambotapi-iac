import os, json
import telebot

from flask import Flask, request

TOKEN = os.environ.get('TOKEN')
URL = os.environ.get('URL')
bot = telebot.TeleBot(TOKEN)
server = Flask(__name__)

knownUsers = {}

commands = {  # command description used in the "help" command
    'start': '(Re)Inicia a conversa',
    'help' : 'Mostra informações dos comandos disponíveis',
}

@bot.message_handler(commands=['start'])
def start(message):
    knownUsers[message.from_user.username] = message.chat.id
    hello_msg = 'Olá, ' + message.from_user.first_name
    bot.send_message(message.chat.id, hello_msg)
    command_help(message)

@bot.message_handler(commands=['help'])
def command_help(message):
    cid = message.chat.id
    help_text = "Os seguintes comandos estão disponíveis: \n"
    for key in commands:
        help_text += "/" + key + ": "
        help_text += commands[key] + "\n"
    bot.send_message(cid, help_text)

@bot.message_handler(func=lambda message: True, content_types=['text'])
def echo_message(message):
    bot.reply_to(message, message.text)

@server.route('/<username>', methods=['POST'])
def send_message(username):
    data = request.get_json()

    if isinstance(data, str):
        data = json.loads(data)

    if username in knownUsers and 'text' in data:
        cid = knownUsers[username]
        bot.send_message(cid, data['text'])
        return "Mensagem enviada com sucesso!", 200

    return "Usuário inexistente ou sem mensagem", 400


@server.route('/' + TOKEN, methods=['POST'])
def getMessage():
    json_string = request.get_data().decode('utf-8')
    update = telebot.types.Update.de_json(json_string)
    bot.process_new_updates([update])
    return "!", 200

@server.route("/")
def webhook():
    bot.remove_webhook()
    bot.set_webhook(url='{}/{}'.format(URL, TOKEN))
    return "!", 200

if __name__ == "__main__":
    server.run(host="0.0.0.0", port=int(os.environ.get('PORT', 5000)))
