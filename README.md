# PyTelegramBotAPI-IAC

ToDo

## Running the example locally
You can clone this project and run the docker build locally:

```shell
$ cat env.list
TOKEN=YOUR_TOKEN
URL=YOUR_URL
```

```shell
docker build -t pytelegrambotapi-iac:latest .
docker run -d -p 5000:5000 --env-file env.list pytelegrambotapi-iac
```

### Note
Ports currently supported for Webhooks: 443, 80, 88, 8443

# References Links
https://core.telegram.org/bots/api#setwebhook

https://github.com/eternnoir/pyTelegramBotAPI

https://gitlab.bio.di.uminho.pt/tutorials/python-flask-docker-ci-helloworld-example
